import express from "express";
import router from "./router";

const server = express();
server.use(express.json());

// void createProductsTable();

server.use("/products", router);

export default server;
