import express, { Request, Response } from "express";
import { addProduct, deleteProduct, getAllProducts, readProduct, updateProduct } from "./dao";

const router = express.Router();

router.get("/version", (req: Request, res: Response) => {
    res.send({ version: '1.0.0' });
});

router.post("/", async (req: Request, res: Response) => {
    const { name, price } = req.body;
    const id = await addProduct(name, price);
    res.send({ id, name, price });
});

router.get("/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const product = await readProduct(Number(id));
    res.send(product);
});

router.get("/", async (req: Request, res: Response) => {
    const product = await getAllProducts();
    res.send(product);
});

router.put("/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const { name, price } = req.body;
    const product = await updateProduct(Number(id), name, price);
    res.send(product);
});

router.delete("/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const product = await deleteProduct(Number(id));
    res.send(product);
});


export default router;
